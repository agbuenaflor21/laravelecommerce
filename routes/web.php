<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// user - user / user@gmail.com / 12345678
// admin - admin / admin@gmail.com / 12345678

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/catalog', 'ItemController@index');

// add Item

Route::get('/additem', 'ItemController@create');

// store item

Route::post('/additem', 'ItemController@store');

// delete item
Route::delete('/deleteitem/{id}', 'ItemController@destroy');

// edit item

Route::get('/edititem/{id}', 'ItemController@edit');

// update item
Route::patch('/edititem/{id}', 'ItemController@update');

// Cart CRUD
// Add to cart
Route::post('/addtocart/{id}', 'ItemController@addtocart');


Route::get('/showcart', 'ItemController@showCart');