@extends("layouts.app")
@section("content")

<h1 class="text-center py-5">Menu</h1>
@if(Session::has("message"))
	<h4>{{ Session::get('message') }}</h4>
@endif

<div class="container">
	<div class="row">
		@foreach($items as $indiv_item)
		<div class="col-lg-4 my-2">
			<div class="card">
				<img class="card-img" src="{{ $indiv_item->imgPath }}" alt="Nothing" height="300px">
				<div class="card-body">
					<h4 class="card-title">{{ $indiv_item->name }}</h4>
					<p class="card-text">{{ $indiv_item->price }}	</p>
					<p class="card-text">{{ $indiv_item->description }}	</p>
					<p class="card-text">{{ $indiv_item->imgpath}}	</p>
					<p class="card-text">{{ $indiv_item->category->name }}	</p>
				</div>
				<div class="card-footer d-flex">
					{{-- <a href="/deletetask/{{ $indiv_task->id }}" class="btn btn-danger">Delete Task</a> --}}
					<form action="/deleteitem/{{ $indiv_item->id }}" method="POST">
						@csrf
						@method('DELETE')
						<button class="btn btn-danger" type="submit">Delete Item</button>
					</form>
					
						<a href="/edititem/{{ $indiv_item->id }}" class="btn  btn-success">Edit Item</a>
					
					{{-- <form action="/markasdone/{{ $indiv_task->id }}" method="POST">
						@csrf
						@method('PATCH')
						<button class="btn btn-success" type="submit">
							@if($indiv_task->status_id != 1)
							<span>Mark as Pending</span>
							@else
							<span>Mark as Done</span>
							@endif
						</button>
					</form> --}}
				</div>
				<div class="card-footer">
					<form action="/addtocart/{{ $indiv_item->id }}" method="POST">
						@csrf
						<input type="number" name="quantity" class="form-control" value="1">
						<button class="btn btn-primary" type="submit">Add to Cart</button>
						{{-- Activity Start --}}
						

						{{-- Activity End --}}
					</form>
				</div>
				

			</div>
		</div>
		@endforeach
	</div>
</div>



@endsection